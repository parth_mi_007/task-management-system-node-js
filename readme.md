
# Task management system - Node js
- Task management system is help to manage tasks of the project.

## Getting started
clone project
npm install
npm run start

### file structure 🥇 
- app 
  - index.js : app start from here
  - config : config files like db 
  - helpers : helper functionality
  - middleware : middlewares
  - models : models
  - routes : libraries 
      - Routes : all the routes
      - services :  all service or controller functions
      - validators : validations 
- locale
  - Multi Language 
- public
  - Static files 
- test
  - Test cases

### packages 🥈
- express
- cors
- morgan
- body-parser
- multer
- joi 
- mongoose
- google-auth-library
- jsonwebtoken


