const { statusCode, messages } = require("./Response")

// log errors
exports.logErrors = function (err, req, res, next) {
  console.error(`${req.method} ${req.url}p`)
  console.error('body --> ', req.body)
  console.error(err.stack)
  return next(err)
}

//error handler
exports.errorHandler = (error, req, res,next) => {
  res
    .status(statusCode.INTERNALERROR)
    .send({ code: statusCode.INTERNALERROR, message: messages.INTERNALERROR })
}
