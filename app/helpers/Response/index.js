const {statusCode} =require('./statusCode')
const {messages,words} = require('./messages')

module.exports ={
    statusCode,
    messages,
    words
}