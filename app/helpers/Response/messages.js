const messages = {
    LOGIN_SUCCESS: 'Logged in successfully.',
    REGISTER_SUCCESS: 'Registered successfully.',
    CREATE : '## created successfully.',
    DELETE : '## deleted successfully.',
    UPDATE: '## updated successfully.',
    GET : '## get successfully.',
    UNAUTHORIZED: "Authentication failed.",
    INTERNALERROR: "Internal error",
    ROUTE_NOT_FOUND: "Route not found.",
    INVALID_CREDENTIALS: "Please enter valid credentials.",
    USER_EXIST: "User already exists.",
}

const words = {
    USER : 'User',
    TASK : 'Task',
    CATEGORY : 'Category'
}

module.exports = {
    messages,
    words
}