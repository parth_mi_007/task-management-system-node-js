const { statusCode, messages } = require('../helpers/Response')

exports.validate = (schema) => (req,res,next) => {
    console.log('body validate-->', req.body)
    const {
        error
      } = schema.validate(req.body)
      if (error) {
        res.status(statusCode.BADREQUEST)
          .send({code: statusCode.BADREQUEST, message: error.details[0].message})
      } else {
        next()
      }
}

exports.routeHandler = (req, res) => {
  res.status(statusCode.NOTFOUND)
  res.send({ message: messages.ROUTE_NOT_FOUND })
}
