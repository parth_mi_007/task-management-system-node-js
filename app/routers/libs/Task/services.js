const { TaskModel, TaskCategoryModel } = require("../../../models")
const {statusCode,messages,words} = require('../../../helpers/Response')

exports.createTask = async (req, res, next) => {
  try {
    const { title, category } = req.body;
    const createdTask = await TaskModel.create({
      title,
      category,
      tasker: req.user.id,
    })
    res.status(statusCode.SUCCESS).send({
        data : createdTask,
        meta : { 
            code : 1,
            message : messages.CREATE.replace('##', words.TASK)
        } 
    
    })
  } catch (error) {
    next(error,req,res)
  }
};
exports.createTaskCategory = async (req, res, next) => {
  try {
    const { title } = req.body;
    const createdCategory = await TaskCategoryModel.create({ title })

    res.status(statusCode.SUCCESS).send({
        data : createdCategory,
        meta : { 
            code : 1,
            message : messages.CREATE.replace('##', words.CATEGORY)
        } 
    })
  } catch (error) {
    next(error,req,res)
  }
};
exports.getAllTask = async (req, res, next) => {
  try {
    const getAllTask = await TaskModel.find()
    res.status(statusCode.SUCCESS).send({
        data : getAllTask,
        meta : { 
            code : 1,
            message : messages.GET.replace('##', words.TASK)
        } 
    })
  } catch (error) {
    next(error,req,res)
  }
}

exports.getAllCategory = async (req, res, next) => {
  try {
    const getAllCategory = await TaskCategoryModel.find()
    res.status(statusCode.SUCCESS).send({
        data : getAllCategory,
        meta : { 
            code : 1,
            message : messages.GET.replace('##', words.CATEGORY)
        } 
    })
  } catch (error) {
    next(error,req,res)
  }

}

exports.updateTask = async (req, res, next) => {
  try {
    const {taskId , categoryId} = req.body
    const getUpdated = await TaskModel.findByIdAndUpdate({_id:taskId },{category:categoryId},{new:true})

    res.status(statusCode.SUCCESS).send({
        data : getUpdated,
        meta : { 
            code : 1,
            message : messages.UPDATE.replace('##', words.TASK)
        } 
    })
  } catch (error) {
      next(error,req,res)
  }
}