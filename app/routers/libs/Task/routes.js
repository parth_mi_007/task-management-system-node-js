const router = require('express').Router()
const taskController = require('./services')
const {isAuth} = require('../../../middleware/Auth')

router.post('/v1/create',isAuth,taskController.createTask)
router.post('/v1/get-all',isAuth,taskController.getAllTask)
router.post('/v1/update',isAuth,taskController.updateTask)
router.post('/v1/get-all-category',isAuth,taskController.getAllCategory)
router.post('/category/v1/create',isAuth,taskController.createTaskCategory)

module.exports = router