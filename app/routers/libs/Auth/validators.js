const Joi = require("joi");

exports.login = Joi.object().keys({
  email: Joi.string().min(5).required(),
  password: Joi.string().required()
})

exports.gLogin = Joi.object().keys({
  code: Joi.string().required()
})

exports.gRegister = Joi.object().keys({
  code: Joi.string().required()
})

exports.register = Joi.object().keys({
    email: Joi.string().min(5).required(),
    password: Joi.string().required(),
    firstName : Joi.string().required(),
    lastName : Joi.string().required()
})