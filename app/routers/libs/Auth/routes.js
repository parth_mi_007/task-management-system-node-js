const router = require('express').Router()
const AuthController = require('./services')
const validation = require('./validators')
const {validate} = require('../../../helpers/Helper')
const imageUpload = require('../../../middleware/multer')

router.post('/v1/login',validate(validation.login), AuthController.Login)
router.post('/v1/register',imageUpload.single('profile'),validate(validation.register), AuthController.register)
router.post('/v1/google-register',validate(validation.gRegister), AuthController.gRegister)
router.post('/v1/google-login',validate(validation.gLogin), AuthController.glogin)

module.exports = router