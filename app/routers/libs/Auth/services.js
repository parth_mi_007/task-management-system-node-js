const { OAuth2Client } = require("google-auth-library")
const { UserModel } = require("../../../models/index")
const { messages,statusCode } =require('../../../helpers/Response')
const bcrypt = require("bcrypt")
var jwt = require("jsonwebtoken")


const oAuth2Client = new OAuth2Client(
  process.env.CLIENT_ID,
  process.env.CLIENT_SECRET,
  "postmessage"
)

exports.glogin = async (req, res, next) => {
  try {
    const { tokens } = await oAuth2Client.getToken(req.body.code);
    const ticket = await oAuth2Client.verifyIdToken({
      idToken: tokens.id_token,
      audience: process.env.CLIENT_ID,
    });
    const payload = ticket.getPayload();
    const existUser = await UserModel.findOne({
      email: payload.email,
      isGoogleLogin: true,
    }).lean()
    if (!existUser) return res.status(statusCode.SUCCESS).send( { code: 0,message: "google user does not registered"})

    var token = jwt.sign(
      { id: existUser._id, email: existUser.email },
      "shhhhh"
    )
    res.status(statusCode.SUCCESS).send({ data:{ ...existUser,token: token},meta: {code: 1, message: messages.LOGIN_SUCCESS}})
  } catch (error) {
    next(error,req,res)
  }
}

exports.gRegister = async (req, res, next) => {
  try {
    const { tokens } = await oAuth2Client.getToken(req.body.code)
    const ticket = await oAuth2Client.verifyIdToken({
      idToken: tokens.id_token,
      audience: process.env.CLIENT_ID,
    })
    
    const payload = ticket.getPayload()
    const createdUser = await UserModel.create({
      firstName: payload.given_name,
      lastName: payload.family_name,
      email: payload.email,
      profile:payload.picture,
      isGoogleLogin: true,
    })
    res.status(statusCode.SUCCESS).send({ data: createdUser, message: messages.REGISTER_SUCCESS })
  } catch (error) {
    next(error,req,res)
  }
}

exports.Login = async (req, res, next) => {
  try {
    const { email, password } = req.body

    const existUser = await UserModel.findOne({ email }).lean()
    if (!existUser) return res.status(statusCode.SUCCESS).send({
      code: 0,
      message: messages.INVALID_CREDENTIALS
    })
    if(existUser.isGoogleLogin) return res.status(statusCode.SUCCESS).send({
      code: 0,
      message: "It seems like registered with google so, please login with google.",
    })

    const validPassword = bcrypt.compareSync(password, existUser.password)
    if (!validPassword)
      return res.send({ message: messages.INVALID_CREDENTIALS })
    var token = jwt.sign(
      { id: existUser._id, email: existUser.email },
      "shhhhh"
    )
    res.status(statusCode.SUCCESS).send({
      code: 1,
      message: messages.LOGIN_SUCCESS,
      data: {...existUser, token:token}
    })
  } catch (error) {
    next(error,req,res)
  }
}

exports.register = async (req, res, next) => {
  try {
    console.log('req.files[0]' ,"--",)
    const { email, password, firstName, lastName } = req.body

    const existUser = await UserModel.findOne({ email })
    if (existUser) return res.status(statusCode.SUCCESS).send({ message:messages.USER_EXIST })

    const hashedPassword = bcrypt.hashSync(password, 10)
    
    const createdUser = await UserModel.create({
      email,
      password: hashedPassword,
      firstName,
      lastName,
      profile:req.file.path
    })
    res.status(statusCode.SUCCESS).send({
      code: 1,
      message: messages.REGISTER_SUCCESS,
      data: createdUser
    })
  } catch (error) {
    next(error,req,res)
  }
}
