const router = require('express').Router()
const taskRoutes = require('./libs/Task/routes')
const userRoutes = require('./libs/Auth/routes')

router.use('/task',taskRoutes)
router.use('/auth',userRoutes)

module.exports = router