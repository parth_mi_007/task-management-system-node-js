require('dotenv').config()
const express = require('express')
const app = express() 
const port = process.env.PORT || 5000
const cors = require('cors')
const morgan = require('morgan')
const bodyParser = require('body-parser')
//db connection
require('./config/db.config')
//enable CORS
app.use(cors())
//HTTP request logger 
app.use(morgan('tiny'))
app.use(express.json())
app.use(express.static('public'))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
//routes
app.use('/',require('./routers'))

// route handlers
app.use('*', require('./helpers/Helper').routeHandler)

//log errors
app.use(require('./helpers/errorHandler').logErrors)

//error handler
app.use(require('./helpers/errorHandler').errorHandler)

//server listen
app.listen(port , () =>{
    console.log('server listening on port', port)
})