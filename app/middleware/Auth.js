const jwt = require('jsonwebtoken')

exports.isAuth = (req,res,next) =>{
    const token = req.headers['x-access-token'] || req.headers['authorization']
    if (!token) return res.status(401).send({code : 401, message : "Access denied. No token provided."})
        try {
            const decoded = jwt.verify(token,'shhhhh')
            req.user = decoded
            next()
        } catch (error) {
            res.status(401).send({code : 401, message : "Invalid token."})
        }
}