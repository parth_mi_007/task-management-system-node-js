const mongoose = require('mongoose')

const TaskCategorySchema = mongoose.Schema({
    title : String
}, { timestamps: true })

module.exports = mongoose.model('TaskCategory', TaskCategorySchema)