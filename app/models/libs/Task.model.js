const mongoose = require('mongoose')

const TaskSchema = mongoose.Schema({
    title : String,
    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'TaskCategory',
        required:true
    },
    tasker: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required:true
    },
}, { timestamps: true })

module.exports = mongoose.model('Task', TaskSchema)