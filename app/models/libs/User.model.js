const mongoose = require('mongoose')

const UserSchema = mongoose.Schema({
    firstName : String,
    lastName : String,
    email : String,
    password : String,
    deletedAt : Date,
    profile: String,
    isGoogleLogin : {
        type : Boolean,
        default: false
    }
}, { timestamps: true })

module.exports = mongoose.model('User', UserSchema)