const TaskModel = require('./libs/Task.model')
const TaskCategoryModel = require('./libs/TaskCategory.model')
const UserModel = require('./libs/User.model')


module.exports ={
    UserModel,
    TaskModel,
    TaskCategoryModel
}